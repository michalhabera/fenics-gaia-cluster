#!/bin/bash
source env-build-fenics.sh

source $HOME/.local/bin/virtualenvwrapper.sh
workon fenics-${TAG}

export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}
FENICS_VERSION="2017.2.0"
FFC_VERSION="2017.2.0.post0"

mkdir -p $BUILD_DIR

cd $BUILD_DIR && \
    git clone https://bitbucket.org/fenics-project/ffc.git && \
    cd ffc && \
    git checkout ${FFC_VERSION} && \
    ${FENICS_PYTHON} setup.py install

sleep 5

cd $BUILD_DIR && \
    git clone https://bitbucket.org/fenics-project/dijitso.git && \
    cd dijitso && \
    git checkout ${FENICS_VERSION} && \
    ${FENICS_PYTHON} setup.py install

sleep 5

cd $BUILD_DIR && \
   git clone https://bitbucket.org/fenics-project/ufl.git
   cd ufl && \
   git checkout ${FENICS_VERSION} && \
   ${FENICS_PYTHON} setup.py install

sleep 5

cd $BUILD_DIR && \
   git clone https://bitbucket.org/fenics-project/instant.git && \
   cd instant && \
   git checkout ${FENICS_VERSION} && \
   ${FENICS_PYTHON} setup.py install

sleep 5

cd $BUILD_DIR && \
   git clone https://bitbucket.org/fenics-project/fiat.git && \
   cd fiat && \
   git checkout ${FENICS_VERSION} && \
   ${FENICS_PYTHON} setup.py install

sleep 5

USE_PYTHON3=$(${FENICS_PYTHON} -c "import sys; print('OFF' if sys.version_info.major == 2 else 'ON')")
cd $BUILD_DIR && \
    git clone https://bitbucket.org/fenics-project/dolfin.git && \
    cd dolfin && \
    git checkout ${FENICS_VERSION} && \
    mkdir -p build && \
    cd build && \
    cmake ../ -DDOLFIN_ENABLE_DOCS=False -DCMAKE_BUILD_TYPE=Release -DSLEPC_INCLUDE_DIRS=${PREFIX}/include -DPETSC_INCLUDE_DIRS=${PREFIX}/include -DSWIG_EXECUTABLE:FILEPATH=${PREFIX}/bin/swig -DEIGEN3_INCLUDE_DIR:FILEPATH=${PREFIX}/include/eigen3 -DCMAKE_INSTALL_PREFIX=${PREFIX} -DDOLFIN_USE_PYTHON3=${USE_PYTHON3} -DPYTHON_EXECUTABLE:FILEPATH=$(which ${FENICS_PYTHON}) && \
    make -j ${BUILD_THREADS} && \
    make install
