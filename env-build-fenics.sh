#!/bin/bash
if [[ $(hostname) =~ "gaia" ]]; then
    FENICS_CLUSTER="gaia"   
elif [[ $(hostname) =~ "iris" ]]; then
    FENICS_CLUSTER="iris"
else
    echo "Do not recognise uni.lu cluster you are running on."
    exit 1
fi

module purge

# OSS compilers
#FENICS_TOOLCHAIN="oss"
#module load toolchain/foss/2017a
#module load lang/Python/3.6.0-foss-2017a-bare
#module load devel/CMake/3.7.2-GCCcore-6.3.0
#module load data/HDF5/1.8.17-foss-2017a
#module load devel/Boost/1.61.0-foss-2017a
#module load math/Eigen/3.3.3-foss-2017a

# Intel compilers
FENICS_TOOLCHAIN="intel"
module load lang/flex/2.6.3
module load devel/Doxygen/1.8.13-GCCcore-6.3.0
module load toolchain/iimpi/2017a
module load lang/Python/3.5.3-intel-2017a
module load devel/CMake/3.8.1-intel-2017a
module load data/HDF5/1.10.0-patch1-intel-2017a
module load devel/Boost/1.63.0-intel-2017a
module load numlib/OpenBLAS/0.2.19-GCC-6.3.0-2.27-LAPACK-3.7.0

# For DOLFIN
export CC=mpigcc
export CXX=mpigxx
export CFLAGS="-lpthread -lm -ldl -lstdc++"
export CXXFLAGS="-lpthread -lm -ldl -lstdc++"
export LDFLAGS="-lpthread -lm -ldl -lstdc++"

# PREFIX is where the root of the install will be. $WORK or $HOME are good
# choices, or $STORE if your happy only to run FEniCS on gaia-80.  BUILD_DIR is
# where everything will be built. If you build on gaia-80, $STORE is a good
# choice.  Make sure you have created your /store directory at
# /store/rues/your-username on gaia-80 and set the variable $STORE in your
# .profile.

FENICS_PYTHON=python3.5

TAG=iris-2017.2.0.r1
PREFIX=${HOME}/fenics-${TAG}
WORKON_HOME=${PREFIX}/virtualenv
BUILD_DIR=/tmp/${USER}/fenics-${TAG}
BUILD_THREADS=1
alias make="make -j${BUILD_THREADS}"

export PATH=${PREFIX}/bin:${PATH}
export LD_LIBRARY_PATH=${PREFIX}/lib:${LD_LIBRARY_PATH}
export C_INCLUDE_PATH=${PREFIX}/include:${C_INCLUDE_PATH}
export CPLUS_INCLUDE_PATH=${PREFIX}/include:${CPLUS_INCLUDE_PATH}

FENICS_PYTHON_VERSION=$(${FENICS_PYTHON} -c 'import sys; print(str(sys.version_info[0]) + "." + str(sys.version_info[1]))')
FENICS_PYTHON_MAJOR_VERSION=$(${FENICS_PYTHON} -c 'import sys; print(str(sys.version_info[0]))')
FENICS_PYTHON_MINOR_VERSION=$(${FENICS_PYTHON} -c 'import sys; print(str(sys.version_info[1]))')
export PYTHONPATH=${PREFIX}/lib/python${FENICS_PYTHON_VERSION}/site-packages:${PYTHONPATH}
