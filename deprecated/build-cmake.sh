#!/bin/bash
source env-build-fenics.sh

mkdir -p $BUILD_DIR

cd $BUILD_DIR && \
    wget --read-timeout=10 -nc https://cmake.org/files/v3.5/cmake-3.5.1.tar.gz
    tar -xf cmake-3.5.1.tar.gz && \
    cd cmake-3.5.1 && \
    ./bootstrap --prefix=${PREFIX} && \
    make -j${BUILD_THREADS} && \
    make install
