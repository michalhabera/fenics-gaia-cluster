#!/bin/bash -l
#OAR -l /enclosure=1/nodes=2/core=4,walltime=0:10:00
#OAR -p cputype='xeon-haswell' OR cputype='xeon-ivybridge' AND os='debian8'
source $HOME/fenics-gaia-cluster/env-fenics.sh
time mpiexec -n 8 -launcher ssh -launcher-exec oarsh $@
# For gaia-80 e.g.:
##OAR -l cpu=8/core=6,walltime=0:10:00
##OAR --project project_rues
##OAR -t bigmem
##OAR -p cputype='xeon-haswell' OR cputype='xeon-ivybridge' AND os='debian8'
# For general cluster use, e.g.:
##OAR -l /enclosure=1/nodes=2/core=4,walltime=0:10:00
##OAR -p cputype='xeon-haswell' OR cputype='xeon-ivybridge' AND os='debian8'
