#!/bin/bash
source env-build-fenics.sh

export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

cd ${BUILD_DIR}
wget https://bootstrap.pypa.io/get-pip.py
${FENICS_PYTHON} get-pip.py --user

${FENICS_PYTHON} -m pip install --user setuptools 
${FENICS_PYTHON} -m pip install --user setuptools virtualenv
${FENICS_PYTHON} -m pip install --user setuptools virtualenvwrapper

source $HOME/.local/bin/virtualenvwrapper.sh
mkvirtualenv --python=${FENICS_PYTHON} fenics-${TAG}
workon fenics-${TAG}

${FENICS_PYTHON} -m pip install --timeout 100 --no-cache-dir numpy numexpr ply six sympy mpi4py pandas ipython ipyparallel petsc4py slepc4py
