#!/bin/bash
source ${HOME}/fenics-gaia-cluster/env-build-fenics.sh

# Note: These must be unset initially, need to code this in somehow.
export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

# Bring in virtualenv with python package
source $HOME/.local/bin/virtualenvwrapper.sh
workon fenics-${TAG}

# Make sure OpenBLAS only uses one thread
export OPENBLAS_NUM_THREADS=1

export INSTANT_CACHE_DIR=${PREFIX}/.instant
export DIJITSO_CACHE_DIR=${PREFIX}/.dijitso
export INSTANT_ERROR_DIR=${PREFIX}/.instant
